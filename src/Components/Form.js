import React from 'react'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import './form.scss'
import * as yup from 'yup'

const SignupSchema = yup.object().shape({
	firstName: yup.string().required('please enter first name'),
	lastName: yup.string().required('please enter last name'),
	age: yup
		.number()
		.typeError('you must specify a number')
		.positive('Enter valid Age')
		.integer('Enter valid Age')
		.required('age is required'),
	email: yup.string().email().required('please enter an Email'),
	phone: yup
		.number('Enter valid Number')
		.typeError('you must specify a number')
		.positive('Enter positive valid Number')
		.integer('Enter valid Number without any "." ')
		.required(),
})
function Form() {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({
		resolver: yupResolver(SignupSchema),
	})
	const onSubmit = (data) => {
		alert(JSON.stringify(data))
	}

	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<div>
				<label>First Name</label>
				<input {...register('firstName')} />
				{errors.firstName && <p>{errors.firstName.message}</p>}
			</div>
			<div style={{ marginBottom: 10 }}>
				<label>Last Name</label>
				<input {...register('lastName')} />
				{errors.lastName && <p>{errors.lastName.message}</p>}
			</div>
			<div style={{ marginBottom: 10 }}>
				<label>Email</label>
				<input {...register('email')} autoComplete='off' />
				{errors.email && <p>{errors.email.message}</p>}
			</div>
			<div>
				<label>Age</label>
				<input type='number' {...register('age', { valueAsNumber: true })} />
				{errors.age && <p>{errors.age.message}</p>}
			</div>
			<div>
				<label>Phone</label>
				<input type='number' {...register('phone', { valueAsNumber: true })} />
				{errors.phone && <p>{errors.phone.message}</p>}
			</div>
			<button>submit</button>
		</form>
	)
}

export default Form
